package com.example.utscandra;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class Receipt extends AppCompatActivity {

    TextView tv_nama, tv_tanggal_pemesanan, tv_makanan, tv_minuman, tv_jumlah_makanan, tv_jumlah_minuman, tv_sub_total, tv_harga_makanan, tv_harga_minuman, tv_total, tv_pajak, tv_pembayaran, tv_kembalian;
    String nama, tanggal_pemesanan,  makanan, minuman;
    String jumlah_makanan;
    String jumlah_minuman;
    String harga_makanan;
    String harga_minuman;
    String pajak;
    String sub_total;
    String total;
    String pembayaran;
    String kembalian;
    Button back_button;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);

        tv_nama = findViewById(R.id.tv_nama);
        tv_tanggal_pemesanan = findViewById(R.id.tv_tanggal_pemesanan);
        tv_makanan = findViewById(R.id.tv_makanan);
        tv_minuman = findViewById(R.id.tv_minuman);
        tv_jumlah_makanan = findViewById(R.id.tv_jumlah_makanan);
        tv_jumlah_minuman = findViewById(R.id.tv_jumlah_minuman);
        tv_harga_makanan = findViewById(R.id.tv_harga_makanan);
        tv_harga_minuman = findViewById(R.id.tv_harga_minuman);
        tv_sub_total = findViewById(R.id.tv_sub_total);
        tv_pajak = findViewById(R.id.tv_pajak);
        tv_total = findViewById(R.id.tv_total);
        tv_pembayaran = findViewById(R.id.tv_pembayaran);
        tv_kembalian = findViewById(R.id.tv_kembalian);

//        Locale localeID = new Locale("in", "ID");
//        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        if(getIntent().getStringExtra("tanggal_pemesanan") != null){
            tanggal_pemesanan = getIntent().getStringExtra("tanggal_pemesanan");
            tv_tanggal_pemesanan.setText(tanggal_pemesanan);
        }

        if(getIntent().getStringExtra("nama") != null){
            nama = getIntent().getStringExtra("nama");
            tv_nama.setText(nama);
        }

        if(getIntent().getStringExtra("makanan") != null){
            makanan = getIntent().getStringExtra("makanan");
            tv_makanan.setText(makanan);

        }

        if(getIntent().getStringExtra("minuman") != null){
            minuman = getIntent().getStringExtra("minuman");
            tv_minuman.setText(minuman);
        }

        if(getIntent().getStringExtra("harga_minuman") != null){
            harga_minuman = getIntent().getStringExtra("harga_minuman");
            tv_harga_minuman.setText(harga_minuman);
        }

        if(getIntent().getStringExtra("harga_makanan") != null){
            harga_makanan = getIntent().getStringExtra("harga_makanan");
            tv_harga_makanan.setText(harga_makanan);
        }

        if(getIntent().getStringExtra("jumlah_minuman") != null){
            jumlah_minuman = getIntent().getStringExtra("jumlah_minuman");
            tv_jumlah_minuman.setText(jumlah_minuman);
        }

        if(getIntent().getStringExtra("jumlah_makanan") != null){
            jumlah_makanan = getIntent().getStringExtra("jumlah_makanan");
            tv_jumlah_makanan.setText(jumlah_makanan);
        }

        if(getIntent().getStringExtra("sub_total") != null){
            sub_total = getIntent().getStringExtra("sub_total");
            tv_sub_total.setText(sub_total);
        }

        if(getIntent().getStringExtra("pajak") != null){
            pajak = getIntent().getStringExtra("pajak");
            tv_pajak.setText(pajak);
        }

        if(getIntent().getStringExtra("total") != null){
            total = getIntent().getStringExtra("total");
            tv_total.setText(total);
        }

        if(getIntent().getStringExtra("pembayaran") != null){
            pembayaran = getIntent().getStringExtra("pembayaran");
            tv_pembayaran.setText(pembayaran);
        }

        if(getIntent().getStringExtra("kembalian") != null){
            kembalian = getIntent().getStringExtra("kembalian");
            tv_kembalian.setText(kembalian);
        }

        //back button
        back_button = findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Receipt.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}