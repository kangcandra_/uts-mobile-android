package com.example.utscandra;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import android.graphics.Color;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;

    TextView et_tanggal_pemesanan;
    Button order_button, buy_button, print_button;
    EditText et_nama, et_jumlah_makanan, et_jumlah_minuman, et_sub_total, et_pajak, et_total, et_pembayaran, et_kembalian;

    Spinner sp_makanan, sp_minuman;

    String nama;
    String tanggal_pemesanan;
    String makanan;
    String minuman;
    String jumlah_makanan;
    String jumlah_minuman;
    String harga_makan;
    String harga_minum;
    String pembayaran;



    int sub_total = 0;
    int total_makan = 0;
    int total_minum = 0;
    double pajak = 0;
    double total = 0;
    int uang_pembayaran = 0;
    double kembalian = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        et_tanggal_pemesanan = findViewById(R.id.et_tanggal_pemesanan);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy");
        String currentDateandTime = sdf.format(new Date());
        et_tanggal_pemesanan.setText(currentDateandTime);
        et_tanggal_pemesanan.setEnabled(false);

        //send data
        order_button = (Button) findViewById(R.id.order_button);
        buy_button = (Button) findViewById(R.id.buy_button);
        print_button = (Button) findViewById(R.id.print_button);
        et_nama = (EditText) findViewById(R.id.et_nama);
        et_tanggal_pemesanan = (EditText) findViewById(R.id.et_tanggal_pemesanan);
        sp_makanan = (Spinner)findViewById(R.id.sp_makanan);
        sp_minuman= (Spinner)findViewById(R.id.sp_minuman);
        et_jumlah_makanan = (EditText) findViewById(R.id.et_jumlah_makanan);
        et_jumlah_minuman = (EditText) findViewById(R.id.et_jumlah_minuman);
        et_sub_total = (EditText) findViewById(R.id.et_sub_total);
        et_pajak = (EditText) findViewById(R.id.et_pajak);
        et_total = (EditText) findViewById(R.id.et_total);
        et_pembayaran = (EditText) findViewById(R.id.et_pembayaran);
        et_kembalian = (EditText) findViewById(R.id.et_kembalian);

        //readonly
        et_sub_total.setEnabled(false);
        et_pajak.setEnabled(false);
        et_total.setEnabled(false);
        et_kembalian.setEnabled(false);


        order_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                nama = et_nama.getText().toString();
                tanggal_pemesanan = et_tanggal_pemesanan.getText().toString();
                makanan = sp_makanan.getSelectedItem().toString();
                minuman = sp_minuman.getSelectedItem().toString();
                jumlah_makanan = et_jumlah_makanan.getText().toString();
                jumlah_minuman = et_jumlah_minuman.getText().toString();

                if (makanan.equals("Honey Garlic Chicken Rice")) {
                    harga_makan = "35000";
                } else if(makanan.equals("Beef Burger")) {
                    harga_makan = "30000";
                } else {
                    harga_makan = "25000";
                }

                if (minuman.equals("Ice Cream Cone")){
                    harga_minum = "10000";
                } else if (minuman.equals("Flurry Oreo")){
                    harga_minum = "18000";
                } else {
                    harga_minum = "15000";
                }

                if(nama.length() == 0 || jumlah_makanan.length() == 0 || jumlah_minuman.length() == 0) {
                    Toast.makeText(MainActivity.this,  "Data Tidak Boleh Kosong!",  Toast.LENGTH_LONG).show();
                } else {
                    // convert to integer
                    int jml_makan = Integer.parseInt(jumlah_makanan);
                    int jml_minum = Integer.parseInt(jumlah_minuman);
                    int hrg_makan = Integer.parseInt(harga_makan);
                    int hrg_minum = Integer.parseInt(harga_minum);
                    total_makan = hrg_makan * jml_makan;
                    total_minum = hrg_minum * jml_minum;
                    sub_total = total_makan + total_minum;
                    pajak = sub_total * 0.1;
                    total = sub_total + pajak;

                    //output
                    et_sub_total.setText("" + String.valueOf(sub_total));

                    et_pajak.setText("" + String.valueOf(pajak));
                    et_total.setText("" + String.valueOf(total));
                }
            }
        });

        buy_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pembayaran = et_pembayaran.getText().toString();
                uang_pembayaran = Integer.parseInt(pembayaran);
                kembalian = uang_pembayaran - total;
                if(nama.length() == 0 || jumlah_makanan.length() == 0 || jumlah_minuman.length() == 0 || pembayaran.length() == 0) {
                    Toast.makeText(MainActivity.this,  "Silahkan Isi Orderan Anda!",  Toast.LENGTH_LONG).show();
                }
                else if(uang_pembayaran < total)
                {
                    Toast.makeText(MainActivity.this,  "Maaf Uang Yang Anda Masukan Kurang!",  Toast.LENGTH_LONG).show();
                }else {
                    et_kembalian.setText("" + String.valueOf(kembalian));
                }
            }
        });

        print_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Receipt.class);
                i.putExtra("nama",nama);
                i.putExtra("tanggal_pemesanan",tanggal_pemesanan);
                i.putExtra("makanan",makanan);
                i.putExtra("minuman",minuman);
                i.putExtra("harga_makanan",String.valueOf(harga_makan));
                i.putExtra("harga_minuman",String.valueOf(harga_minum));
                i.putExtra("jumlah_makanan",jumlah_makanan);
                i.putExtra("jumlah_minuman",jumlah_minuman);
                i.putExtra("sub_total",String.valueOf(sub_total));
                i.putExtra("pajak", String.valueOf(pajak));
                i.putExtra("total", String.valueOf(total));
                i.putExtra("pembayaran", String.valueOf(uang_pembayaran));
                i.putExtra("kembalian", String.valueOf(kembalian));
                startActivity(i);

            }
        });



    }
}